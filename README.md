# Lost in Translation
A sign language translator made with React. This sign language translator allows users to translate characters in a word to their corresponding signs. Users also have access to a profile page where previous translations are displayed. To get the users and their translation history and API is used to fetch and post data.
***
## Install

To install dependencies, run
```
clone repo
cd into project root folder
npm install
```

## Usage

Add a .env file to project root.
In the .env file add the following:

```
REACT_APP_API_KEY= <your key>
REACT_APP_API_URL= <your url>
```


## Contributors
- [ChristofferMalmberg](@ChristofferMalmberg)
- [MartinaBlixtEriksson](@MartinaBlixtEriksson)
