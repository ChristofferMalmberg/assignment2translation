import { createContext, useContext, useState } from "react"
import { storageRead } from "../utils/storage"


const UserContext = createContext()

export const useUser = () => {
    return useContext(UserContext)
}


function UserProvider(props){

    //TODO set better key
    const [ user, setUser ] = useState(storageRead("Translation-user"))

    const state = {
        user,
        setUser
    }


    return(
        <UserContext.Provider value={ state }>
            {props.children}
        </UserContext.Provider>
    )
}

export default UserProvider