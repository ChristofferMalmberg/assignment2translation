const apiKey = process.env.REACT_APP_API_KEY

//Crates headers used for posting data to the api
export const createHeaders = () => {
    return {
        'Content-Type': 'application/json',
        'x-api-key': apiKey
    }
}