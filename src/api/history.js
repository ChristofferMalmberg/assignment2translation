import { createHeaders } from "."

const apiUrl = process.env.REACT_APP_API_URL


//Retrieves the translation history for the current user
export async function getHistory(user){
    try {
        const response = await fetch(`${apiUrl}/${user.id}`)
        if(!response.ok) throw new Error("Could not retrieve translation history for current user")
        const data = await response.json()
        const { translations } = data
        return translations
    } catch (error) {
        return [ error.message, null ]
    }
}

//Updates the translation history for the current user
export async function addHistory(user, translation){
    try {
        console.log(user.translations)
        const response = await fetch(`${apiUrl}/${user.id}`, {
            method: "PATCH",
            headers: createHeaders(),
            body: JSON.stringify({
                username: user.username,
                translations: [...user.translations, translation]
            })
        })
        if(!response) throw new Error("Could not update history")

        const result = await response.json()
        return [ null, result ]

    } catch (error) {
        return [ error.message, null ]
    }
}

//Removes the translation history for the current user
export const translationClearHistory = async (userId) => {
    try {
        const response = await fetch(`${apiUrl}/${userId}`, {
            method: 'PATCH',
            headers: createHeaders(),
            body: JSON.stringify({ translations: []})
        })
        if(!response.ok){
            throw new Error('Could not update history')
        }
        const result = await response.json()
        return [ null, result ]
    } catch (error) {
        return [ error.message, null ]
    }
}
