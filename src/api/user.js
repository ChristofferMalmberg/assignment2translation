import { createHeaders } from "."

const apiUrl = process.env.REACT_APP_API_URL

export async function checkForUser(username){
    try {
        //try to fetch username
        console.log(`${apiUrl}$username=${username}`);
        const response = await fetch(`${apiUrl}?username=${username}`)
        if(!response.ok) throw new Error("Couldn't retrieve user")
        const data = await response.json()
        return [ null, data ] //Return null for the error parameter, and the fetched data as second one
        
    } catch (error) {
        return [ error.message, null ]
    }
}

//Creates a new user with the provided username
export async function createUser(username){
    try {
        const response = await fetch(apiUrl, {
            method: "POST",
            headers: createHeaders(),
            body: JSON.stringify({
                username,
                translations: []
            })
        })
        if(!response.ok) throw new Error("Could not create user")
        const data = await response.json()
        return [ null, data ]
        
    } catch (error) {
        return [ error.message, null ]
    }
}

//Checks for user and if exist log in, otherwise create new user
export async function loginUser(username){

    const [ errorMsg, user ] = await checkForUser(username)
    if( errorMsg !== null ) return [ errorMsg, null ]
    if(user.length > 0) return [ null, user.pop() ]    
    return await createUser(username)
}

export const userById = async (userId) => {
    try {
        const response = await fetch(`${apiUrl}/${userId}`)
        if (!response.ok) {
            throw new Error("Could not find user")
        }
        const user = await response.json()
        return [null, user]
    }
    catch (error) {
        return [ error.message, null ]
    }
}
