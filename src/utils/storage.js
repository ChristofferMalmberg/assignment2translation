

//Save (user) to the session storage 
export const storageSave = (key, value) => {
    if(!key && typeof key !== 'string'){
        throw new Error('No storage key provided')
    }

    if(!value){
        throw new Error('No value provided for', key)
    }
    sessionStorage.setItem(key, JSON.stringify(value))
}

//Read (user) from session storage
export const storageRead = (key) => {
    const userData = sessionStorage.getItem(key)
    if(userData) return JSON.parse(userData)
    return null
}

//Delete (user) from the session storage
export const storageDelete = (key) => {
    sessionStorage.removeItem(key)
}