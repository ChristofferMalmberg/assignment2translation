import { Navigate } from "react-router-dom"
import { useUser } from "../context/UserContext"

//(Protects Routes) Takes a component as input and checks that user is logged in
//Otherwise redirect to log in page
const withAuth = Component => props => {

    const { user } = useUser()
    if(user !== null){
        return <Component {...props} />
    }
    else{
        return <Navigate to='/' />
    }

}

export default withAuth