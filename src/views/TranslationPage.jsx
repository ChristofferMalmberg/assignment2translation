import { useState } from "react";
import TranslationInput from "../components/Translation/TranslationInput";
import TranslationOutput from "../components/Translation/TranslationOutput";
import withAuth from "../hoc/withAuth";
import "../components/Translation/Translation.css";

function TranslationPage() {
  const [translationText, setTranslationText] = useState([]);

  return (
    <>
      <TranslationInput setText={setTranslationText} />
      <div className="flex-container">
        <div className="translation-container">
          <TranslationOutput characters={translationText} />
          {translationText.map((character, index) => (
            <TranslationOutput
              key={index + " " + character}
              character={character}
            />
          ))}
        </div>
      </div>
    </>
  );
}

export default withAuth(TranslationPage);
