import '../App.css'

function NotFoundPage() {
    return(
        <div className="errorPage">
            <h1>404</h1>
            <h2>Not found</h2>
            <p>The requested page could not be found</p>
        </div>
    )
}

export default NotFoundPage
