import LoginForm from "../components/Login/LoginForm";
import "../components/Login/LoginForm.css";
import logo from '../img/Logo.png'


function StartupPage() {
  return (
    <>
      <header className="login-header">
        <img className="header-img" src={ logo } alt="Logo"></img>
        <h1 className="heading"> Lost in Translation</h1>
      </header>
      <LoginForm />
    </>
  );
}

export default StartupPage;
