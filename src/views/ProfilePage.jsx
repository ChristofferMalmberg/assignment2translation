import ProfileActions from "../components/Profile/ProfileActions"
import ProfileHeader from "../components/Profile/ProfileHeader"
import ProfileTranslationHistory from "../components/Profile/ProfileTranslationHistory"
import '../components/Profile/Profile.css'
import withAuth from "../hoc/withAuth"
import { useUser } from "../context/UserContext"



function ProfilePage() {

    const  { user } = useUser()

    return(
        <>
            <ProfileHeader username={ user.username } />
            <div className="profile-body">
                <ProfileTranslationHistory translations={user.translations} />
                <ProfileActions />
            </div>
        </>
    )
}

export default withAuth(ProfilePage)
