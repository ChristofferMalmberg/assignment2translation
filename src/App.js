import { BrowserRouter, Routes, Route } from "react-router-dom";
import "./App.css";
import Navbar from "./components/Navbar/Navbar";
import NotFoundPage from "./views/NotFoundPage";
import ProfilePage from "./views/ProfilePage";
import StartupPage from "./views/StartupPage";
import TranslationPage from "./views/TranslationPage";

//Comment to redeploy

function App() {
  return (
    <BrowserRouter>
      <div className="grid-container">
        <div className="App">
          <Navbar />
          <Routes>
            <Route path="/" element={<StartupPage />} />
            <Route path="/profile" element={<ProfilePage />} />
            <Route path="/translation" element={<TranslationPage />} />
            <Route path="*" element={<NotFoundPage />} />
          </Routes>
        </div>
      </div>
    </BrowserRouter>
  );
}

export default App;
