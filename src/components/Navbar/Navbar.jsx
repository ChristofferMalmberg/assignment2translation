import { useUser } from "../../context/UserContext";
import { NavLink } from "react-router-dom";
import "../Navbar/navbar.css";

function Navbar() {
  const { user } = useUser();

  return (
    <>
      <nav className="container">
        <ul>
          <li className="heading">Lost in Translation</li>
        </ul>

        {user !== null && (
          <span className="navLinks">
            <NavLink to="/translation">Translation</NavLink>
          </span>
        )}

        {user !== null && (
          <span className="navLinks">
            {" "}
            <NavLink to="/profile">{user.username}</NavLink>{" "}
          </span>
        )}
      </nav>
    </>
  );
}

export default Navbar;
