import { useEffect } from "react";
import { useForm } from "react-hook-form";
import { loginUser } from "../../api/user";
import { useUser } from "../../context/UserContext";
import { storageSave } from "../../utils/storage";
import { useNavigate } from "react-router-dom";
import { STORAGE_KEY_USER } from "../../const/storageKeys";


const usernameConfig = {
  required: true,
};

function LoginForm() {
  //Hooks
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const { user, setUser } = useUser();
  const navigate = useNavigate();

  //If user is logged in, "redirect" to translation page
  useEffect(() => {
    if (user !== null) {
      navigate("/translation");
    }
  }, [user, navigate]);

  //onSubmitHandle receives an object, and username is destructured
  const onSubmitHandle = async ({ username }) => {
    const [errorMsg, userResponse] = await loginUser(username);

    if (errorMsg !== null) console.log("Error");
    if (userResponse !== null) {
      setUser(userResponse);
      storageSave(STORAGE_KEY_USER, userResponse);
    }
  };

  return (
    <>
      <div className="login-container">
        <form>
          <input
            type="text"
            className="login-input"
            placeholder="What's your name?"
            {...register("username", usernameConfig)}
          />
          <button onClick={handleSubmit(onSubmitHandle)}>Login</button>
        </form>
        {errors.username && errors.username.type === "required" && (
          <span className="login-error">Username is required to login</span>
        )}
      </div>

    </>
  );
}

export default LoginForm;
