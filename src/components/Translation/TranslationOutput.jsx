

const TranslationOutput = ({ character }) => {

    const imgSrc = `img/${character}.png`

    return(
        <>
            {character && <img src={imgSrc} alt={character} />}

        </>
    )
}

export default TranslationOutput