import { useRef } from "react"
import { addHistory } from "../../api/history"
import { useUser } from "../../context/UserContext"
import { storageSave } from "../../utils/storage"
import { STORAGE_KEY_USER } from "../../const/storageKeys";


const TranslationInput = ({setText}) => {
    const inputRef = useRef(null)
    const { user, setUser } = useUser()
    const handleOnClick = async (event) => {
        //Prevents form from being submit which would have removed input
        event.preventDefault()
        //Gets current value of input field splits and filters characters to remove non letter input
        let filteredCharacters = inputRef.current.value.toLowerCase().split("").filter(character => /[a-zA-Z]/.test(character))
        console.log(filteredCharacters);
        setText(filteredCharacters)

        //This returns entire user object, which is updated with new history 
        const [error, updatedUser] = await addHistory(user, filteredCharacters.join(""))
        
        if(error !== null) return 

        //Keep ui and server state synced
        storageSave(STORAGE_KEY_USER, updatedUser)
        //Context sync
        setUser(updatedUser)
    }

    return(
        <>
            <div className="translation-input-container">
                <form className="input-form">
                    <label>What do you want to translate?</label>
                    <input ref={inputRef} type="text" placeholder="Input text" maxLength="40"></input>
                    <button onClick={handleOnClick}>Translate</button>
                </form>
            </div>
        </>
    )
}

export default TranslationInput