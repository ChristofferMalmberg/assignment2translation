function ProfileTranslationHistoryItem ({ translation }){
    return(
        <> 
            <li className="translation-item">{ translation }</li>
        </>
    )
}

export default ProfileTranslationHistoryItem