import ProfileTranslationHistoryItem from "./ProfileTranslationHistoryItem"

function ProfileTranslationHistory ({ translations }){

    if(!translations){
        return
    }

    //Map the words from the array and pass them into history item as a prop
    const translationHistory = translations.map((word, index) => {
        return <ProfileTranslationHistoryItem key={ index + "-" + word} translation={ word } />
    })

    return(
        <>
            <div className="history-container">
                <h4>Your previous translations:</h4>
                <ul>
                    {translationHistory.length !== 0 && translationHistory.reverse()}
                    {translationHistory.length === 0 && <p>No history</p> }
                    {translationHistory.length > 10 && translationHistory.reverse()}
                </ul>
            </div>
        </>
    )
}

export default ProfileTranslationHistory