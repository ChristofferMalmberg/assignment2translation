import { useNavigate } from "react-router-dom";
import { translationClearHistory } from "../../api/history";
import { useUser } from "../../context/UserContext";
import { storageDelete, storageSave } from "../../utils/storage";
import { STORAGE_KEY_USER } from "../../const/storageKeys";

function ProfileActions() {
  const { user, setUser } = useUser();
  const navigate = useNavigate();

  //Logs out user, clears session storage, sets current user to null and redirects user to login page
  function handleLogout() {
    if (!window.confirm("Are you sure you want to log out?")) {
      return;
    }
    storageDelete("Translation-user");
    setUser(null);
    navigate("/");
  }


  //Clears user translation history if user confirms the alert
  async function handleRemoveTranslationHistory() {
    if (!window.confirm("Are you sure you want to delete all history?")) {
      return;
    }

    const [clearError, result] = await translationClearHistory(user.id);

    if (clearError !== null) {
      return;
    }

    storageSave(STORAGE_KEY_USER, result);
    setUser(result);
  }

  return (
    <div className="actions">
      {user.translations && (
        <button className="blockBtn" onClick={handleRemoveTranslationHistory}>
          Clear translation history
        </button>
      )}
      <button className="blockBtn" onClick={handleLogout}>
        Logout
      </button>
    </div>
  );
}

export default ProfileActions;
