import logo from '../../img/Logo-Hello.png'

function ProfileHeader ({ username }){


    return(
        <>
            <header>
                <img className='header-img' src={ logo } alt="logo"></img>
                <h1 className="profile-heading">Welcome, { username }</h1>
            </header>
        </>
    )
}

export default ProfileHeader